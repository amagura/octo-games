'use strict';
var mongoose = require('mongoose')
  ;

var gameSchema = new mongoose.Schema({
  name: {
    type: String,
    index: {
      required: true,
      unique: true
    }
  },
  developers: {
    type: [String],
    index: { required: true }
  },
  dateAdded: {
    type: Date,
    index: { required: true }
  },
  releaseDate: {
    type: Date,
    index: { required: true }
  },
  platform: {
    type: String,
    index: { required: true }
  },
  genres: {
    type: [String],
    index: { required: true }
  },
  ESRB: { // is always present by default, therefore, no point in requiring.
    type: mongoose.Schema.Types.Mixed,
    index: true
  },
  rating: {
    type: Number,
    min: 1, // did not like the game, quit playing it as a result.
    max: 10, // loved the game so much that I enjoy revisiting it.
    index: { required: true }
  },
  boxart: {
    type: mongoose.Schema.Types.Mixed
  }
});

var Game = mongoose.model('Game', gameSchema);
Game.saveGame = function(game, cb) {
  game = new Game(game);
  game.save(function(err) {
    if (err) return cb(err);
    cb();
  });
};

module.exports = Game;
