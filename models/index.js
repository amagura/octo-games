'use strict';
var fs    = require('fs')
  , path  = require('path')
  ;

var noTrailingS = true;

var models = {};

fs.readdirSync(__dirname)
.filter(function(file) {
  return file !== 'index.js';
})
.forEach(function(model) {
  var _model = {
    file: model,
    ns: (model[0].toUpperCase() + model.slice(1)).replace(path.extname(model), '')
  };
  if (noTrailingS) _model.ns = _model.ns.replace(/s?$/, '');
  models[_model.ns] = require(__dirname + '/' + _model.file);
});

module.exports = models;
