define(`_newline', `
')dnl
define(`tab', `ifelse($1, 0,, `  '`tab(decr($1))')')dnl
define(`assign', `ifelse($#, 0,, $#, 1,, `$1: $2;'`_newline()'`assign(shift(shift($@)))')')dnl
define(`one_assign', `ifelse($#, 1,, $2, `',, `$2: $1;'`_newline()'`one_assign($1, shift(shift($@)))')')dnl
define(`assign_one', `ifelse($#, 1,, $2, `',, `$1: $2;'`_newline()'`assign_one($1, shift(shift($@)))')')dnl
define(`_sseq', `ifelse($#, 1,, $2, `',, `$1$2,'`_sseq($1, shift(shift($@)))')')dnl
define(`sseq_', `ifelse($#, 1,, $2, `',, `$2$1,'`sseq_($1, shift(shift($@)))')')dnl
define(`_seq', `ifelse($#, 1,, $2, `',, `$1$2,'`_sseq($1, shift(shift($@)))')')dnl
define(`seq_', `ifelse($#, 1,, `$2$1,'`sseq_($1, shift(shift($@)))')')dnl
dnl double_with_space
define(`dbl_wspc', `ifelse($#, 0,, $1, `',, `$1 $1 '`dbl_wspc(shift($@))')')dnl
