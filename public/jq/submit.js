$(document).ready(function() {
  $('#submit-a-game-form').submit(function(e) {
    // prevent Default functionality
    e.preventDefault();
    var values = {};
    //var xhr = new XMLHttpRequest();
    var data = new FormData();
    $.each($('#submit-a-game-form').serializeArray(), function(i, field) {
      data.append(field.name, field.value);
    });
    $.each($('#fileinput').get(0).files, function(i, file) {
      data.append('file-' + i, file);
    });
    //$.ajax({
      //url: '/api/boxart',
      //type: 'post',
      //data: new FormData($('#fileinput')[0]),
      //processData: true,
      //success: function(res) {
        //console.log(res);
      //}
    //});
    //xhr.open('POST', $id('fileinput').action, true);
    //xhr.setRequestHeader('X_FILENAME', file.name);
    //xhr.sendFile

    // get the action-url of the form
    console.log($('#submit-a-game-form').serialize());
    var actionUrl = e.currentTarget.action;
    $.ajax({
      url: actionUrl,
      data: data,
      type: 'post',
      cache: false,
      contentType: false,
      processData: false,
      success: function(res) {
        console.log(res);
        return true;
      }
    });
  });
});
