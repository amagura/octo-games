function addDeveloper() {
  /* get the last entry in the lastelopers array */
  var lastEntry = document.getElementById('last-developer')
    , newEntry = lastEntry.cloneNode()
    , count = lastEntry.name.substr(lastEntry.name.search(/[0-9]/))
    , name = lastEntry.name.substr(lastEntry.name.search(/[^0-9]/), lastEntry.name.search(/[0-9]/))
    ;
  ++count;
  lastEntry.id = name + (count - 1);
  newEntry.name = name + count;
  var _class = document.createAttribute('class');
  _class.value = 'developer-array';
  newEntry.setAttributeNode(_class);
  document.getElementById('form-developer').appendChild(newEntry);
}

function removeDeveloper() {
  /* get the last entry in the lastelopers array */
  //var entries = document.getElementById('form-developer');
  var lastEntry = document.getElementById('last-developer')
    , count = lastEntry.name.substr(lastEntry.name.search(/[0-9]/))
    , name = lastEntry.name.substr(lastEntry.name.search(/[^0-9]/), lastEntry.name.search(/[0-9]/))
    ;

  --count;
  if (count === -1) return; /* prevents you from removing the last node */
  var newEntry = document.getElementById((name + count));
  document.getElementById('form-developer').removeChild(lastEntry);
  var oldEntry = document.getElementById((name + count));
  //console.log('entries:');
  //console.log(entries);
  //console.log('old:');
  //console.log(oldEntry);
  //console.log('new:');
  //console.log(newEntry);
  newEntry.id = 'last-developer';
  document.getElementById('form-developer').replaceChild(newEntry, oldEntry);
}

function autofill() {
  document.getElementById('developer').innerHTML = 'Square Enix';
  [
    [ 'platform', 'Playstation' ],
    [ 'form-name', 'Final Fantasy Tactics' ],
    [ 'rating', 10 ],
    [ 'rel_date', 'January 28, 1998' ]
  ].forEach(function(item) {
    var value = document.createAttribute('value');
    value.value = item[1];
    document.getElementById(item[0]).setAttributeNode(value);
  });
}

function unfill() {
  document.getElementById('developer').innerHTML = '';
  [
    [ 'platform', '' ],
    [ 'form-name', '' ],
    [ 'rating', 1 ],
    [ 'rel_date', '' ]
  ].forEach(function(item) {
    var value = document.createAttribute('value');
    value.value = item[1];
    document.getElementById(item[0]).setAttributeNode(value);
  });
}

function block_input(name, type, _var, id, _class, _required) {
  var res = '<p class=\'submit-form\'>' + name + '</p>';
  res += '<input type=\'' + type + '\' name =\'' + _var + '\'';
  if (id)
    res += ' id=\'' + id + '\'';
  if (_class)
    res += ' class=\'' + _class + '\'';
  if (_required)
    res += ' required';
  res += '>';
  document.write(res);
}
