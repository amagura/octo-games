<?php
function block_input($name, $type, $var, $id = null, $class = null, $required = null) {
  $res = "<p class='submit-form'>$name</p>";
  $res .= "<input type='$type' name='$var'";
  if ($id) $res .= " id='$id'";
  if ($class) $res .= " class='$class'";
  $res .= '>';
  echo $res;
}

function inline_input($name, $type, $var, $id = null, $class = null, $required = null) {
  $res = "<span class='submit-form'>$name</span>";
  $res .= "<input type='$type' name='$var'";
  if ($id) $res .= " id='$id'";
  if ($class) $res .= " class='$class'";
  if ($required) $res .= " required";
  $res .= '>';
  echo $res;
}
