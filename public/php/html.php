<?php
function doctype($type) {
  echo "<!DOCTYPE $type>";
}

function title($title) {
  echo "<title>" . rawurlencode("$title") . "</title>";
}
