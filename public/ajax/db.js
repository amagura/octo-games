function showGame(name) {
  var xmlhttp = (window.XMLHttpRequest
                ? new XMLHttpRequest()
                : new ActiveXObject('Microsoft.XMLHTTP'));
  if (name === '') {
    return;
  }
  xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
      document.write(xmlhttp.responseText);
    }
  }
  xmlhttp.open('GET', '/api/listing?q=' + name, true);
  xmlhttp.send();
}
