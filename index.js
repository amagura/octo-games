var EOL         = require('os').EOL
  , fs          = require('fs')
  , express     = require('express')
  , util        = require('util')
  , formidable  = require('formidable')
  , models      = require('./models')
  , log         = require('./lib/log')
  , connection  = require('./lib/connection').config()
  ;

var app = express()
  , form = new formidable.IncomingForm()
  ;

app.use(express.static(__dirname + '/public/'));
app.set('views', __dirname + '/jade/');
app.set('view engine', 'jade');

function getPage(page) {
  return fs.readFileSync('./public/html/' + page + '.html');
}

app.get('/', function(req, res) {
  routeHits(req.route);
  res.render('index');
});

app.get('/games/submit', function(req, res) {
  routeHits(req.route);
  res.render('submit');
});

app.get('/games', function(req, res) {
  routeHits(req.route);
  res.end(getPage('games'), function(err, rez) {
    if (err) log.error(err);
    connection.open();
    models.Game.find({}, function(err, rez) {
      if (err) log.error(err);
      res.send(rez[0].boxart.buffer, 'binary');
      connection.close();
    });
  });
});

app.get('/youtube', function(req, res) {
  routeHits(req.route);
  res.end(getPage('youtube'), function(err, rez) {
    if (err) log.error(err);
  });
});

app.post('/api/listing', function(req, res) {
  routeHits(req.route);
  connection.open();
  models.Game.find({}, function(err, rez) {
    if (err) throw err;
    res.send(rez);
    connection.close();
  });
});

app.post('/api/submit', function(req, res) {
  routeHits(req.route);
  form.parse(req, function(err, fields, files) {
    if (err) throw err;
    fields.developer = fields.developer.split(EOL)
    .map(function(dev) {
      return dev.replace(/\r|\n/g, '');
    });

    boxart = fs.readFileSync(files['file-0'].path);
    connection.open();
    models.Game.saveGame({
      name: fields.name,
      rating: fields.rating,
      platform: fields.platform,
      ESRB: fields.esrb,
      releaseDate: fields.rel_date,
      developers: fields.developer,
      genres: fields.genre,
      dateAdded: new Date(),
      boxart: boxart
    }, function(err) {
      if (err) throw err;
      models.Game.find({ name: fields.name }, function(err, rez) {
        if (err) throw err;
        res.send();
        connection.close();
      });
    });
  });
});

function routeHits(route) {
  var method = Object.keys(route.methods).filter(function(key) {
    return route.methods[key] === true;
  }).pop().toUpperCase();
  log.silly(method + ' ' + route.path);
}

var server = app.listen(3000, function() {
  console.log('Listening on port %d', server.address().port);
});
