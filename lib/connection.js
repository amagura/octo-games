'use strict';
var mongoose = require('mongoose')
  ;

function Connection(srv, db) {
  this.connection = {};
  return this;
}

Connection.prototype.config = function(srv, db) {
  this._srv = srv || 'localhost';
  this._db  = db || 'alexej';
  return this;
};

Connection.prototype.open = function() {
  var self = this;
  this._mongoose = mongoose.connect('mongodb://' + self._srv + '/' + self._db);
  return true;
};

Connection.prototype.close = function() {
  var self = this;
  this._mongoose.connection.close();
  return true;
};

module.exports = new Connection();
