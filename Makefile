UGLY := /usr/bin/env uglifyjs
SHELL := /usr/bin/env bash
LESSC := /usr/bin/env lessc
JADE := /usr/bin/env jade
M4 := /usr/bin/env m4
startdir = "$(PWD)"

all: less jade run

.PHONY: less jade
less:
	cd less;\
	  for files in *.less; do\
	    $(LESSC) "$$files" $(startdir)/public/css/"$${files//.less/}.css";\
	    done
jade:
	cd jade;\
	  for files in *.jade; do\
	    $(JADE) "$$files" -o $(startdir)/public/html;\
	    done
php:
	@# expand all php stuff in nunjuck templates
	cd html;\
	  for files in *.php; do\
	    $(PHP) "$$files" > "$${files//.php/}.html";\
	  done
run:
	cat <<<'clearing...' > /var/log/node.log
	killall node;\
	  nohup node $(startdir)/index.js &>> /var/log/node.log &

logless:
	less /var/log/node.log

log-clean:
	cat /dev/null > /var/log/node.log

logtail:
	tail /var/log/node.log

correct-package:
	sed -i '/[0-9]\./s/\^/~/' $(startdir)/package.json

clean:
	rm -f $(startdir)/jade/.*html
	rm -f $(startdir)/public/html/*.html
	rm -f $(startdir)/public/css/*.css
tail:
	tail -f /var/log/node.log
